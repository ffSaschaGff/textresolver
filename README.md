Классификатор текстов
======================
В данном проекте REST реализован на Spring, нейросеть реализована на neuroph, в качестве базы данных береться Postgres. Алгоритм работы сводиться к следующим шагам: в сервис сначало загружаются классы, потом загружаются тексты с указанием соответствующих идентификаторов классов (возвращаются в ответе при запросе на добавление нового класса), после этого перезаполняется словарь и обучается нейросеть. После этого можно отправлятьв сервис тексты на распознование.

Нехническию нюансы
------------------
При составлении словаря для того, чтобы отбросить опечатки в словарь попадают только слова, которые встретились более N раз, при распознании включется автоисправление, доспустимо не более 1 ошибки на четыре слова.

Описание маппингов
------------------

Путь              |Тело запроса         |Возвращаемое значение     |Описание
------------------|---------------------|--------------------------|----------
/test             |                     |{"status": "ok"}          |Возвращает {"status": "ok"}
/class/add        |Объект "Класс"       |Объект "Класс"            |Добавляет новый класс, не рекомендуется уставлять поле order вручную, оногенерируется автоматически
/class/getAll     |                     |Массив "Класс"            |Возвращает список всех доступных классов
/text/add         |Объект "Текст"       |Объект "Текст"            |Добавляет новый текст в список текстов, которые будут использоватся для обучения
/text/addList     |Массив "Текст"       |{"status": "finished"}    |Добавляет массив документов
/text/getAll      |                     |Массив "Текст"            |Возвращает список всех текстов, используемых для обучения
/dictionary/refill|                     |{"status": "finished"}    |Перезаполняет словарь, по которому будет составляться вектор текста для входящего слоя нейросети
/settings/getAll  |                     |Массив "Настройки"        |Возвращает все выставленные настройки
/settings/add     |Объект "Настройки"   |{"status": "finished"}    |Добавляет новую настройку
/ann/init         |                     |{"status": "finished"}    |Инициализирует новую пустую нейросеть
/ann/train        |                     |json с итерацией и ошибкой|Обучает нейросеть, записывая после ее в БД
/ann/calculate    |Объект "Текст"       |Массив чисел              |По тексту возвращает веса классов, если нейросеть не заполнена, но была сохранена в БД, то в начале инициализируется данными из БД
/ann/save         |                     |Серилизованная строка     |Возвращает серилизованную нейросеть
/ann/load         |Серилизованная строка|{"status": "finished"}    |Загружает нейросеть по переданным данным

***
Описание объектов
------------------
* Классс:
{"name":"Имя класса","order":"Порядок класса в выводе и для вектора текста, нерекомендован к заполнению","id":"UUID текста, нерекомендован к заполнению"}
* Текст:
{"text":"Классифицируемый текст","class":"UUID класса","id":"UUID текста, нерекомендован к заполнению","words":"Слова словаря, которые используются в тексте, только для чтения"}
* Настройки: {"name":"Имя настрокий","data":"Данные настройки"}

***
Описание настроек
------------------
* ann_data - хранит данные нейросети, записывается после выполнения обучения "/ann/train", загружается при запросе класса "/ann/calculate".
* number_of_layers - определяет количество слоев, которые будут созданые в нейросети, значение по умолчанию - 3
* dictionary_barrier - порог попадания слов в словарь. Если слово во всех текстах встретилось меньшее количество раз, то в словарь оно не попадет. Значение по умолчанию - 3.
* learning_max_iterations - максимальное количество итераций за одно обучение
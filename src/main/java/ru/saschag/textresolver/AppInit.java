package ru.saschag.textresolver;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import ru.saschag.textresolver.conf.DBConfig;
import ru.saschag.textresolver.conf.WebConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * Created by galanov.ag on 28.05.2019.
 */
public class AppInit implements WebApplicationInitializer {

    private final static String DISPATCHER = "dispatcher";

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        Class[] classes = new Class[]{WebConfig.class, DBConfig.class};

        //ctx.register(WebConfig.class);
        ctx.register(classes);


        servletContext.addListener(new ContextLoaderListener(ctx));

        ServletRegistration.Dynamic servlet = servletContext.addServlet(DISPATCHER, new DispatcherServlet(ctx));

        servlet.addMapping("/");

        servlet.setLoadOnStartup(1);

    }

}

package ru.saschag.textresolver.servise;

import ru.saschag.textresolver.entity.Settings;

import java.util.List;

/**
 * Created by galanov.ag on 30.05.2019.
 */
public interface SettingsServise {
    String NAME_ANN_DATA = "ann_data";
    String NAME_LAYERS_COUNT = "number_of_layers";
    String NAME_DICTIONARY_BARRIER = "dictionary_barrier";
    String NAME_LEARNING_MAX_ITERATIONS = "learning_max_iterations";

    Settings getSettings(String name);
    void setSettings(Settings settings);
    List<Settings> getAll();
}

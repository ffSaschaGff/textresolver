package ru.saschag.textresolver.servise.serviseImpl;

import org.springframework.stereotype.Service;
import ru.saschag.textresolver.servise.LevenshteinDistance;

/**
 * Created by galanov.ag on 03.06.2019.
 */
@Service
public class LevenshteinDistanceImpl implements LevenshteinDistance {
    @Override
    public int getDistance(String word1, String word2) {
        if (word1.equals(word2)) {
            return 0;
        }
        if (word1.length() == 0) {
            return word2.length();
        }
        if (word2.length() == 0) {
            return word1.length();
        }
        return getDistanceReqursive(word1,word2,word1.length()-1,word2.length()-1, 0);
    }

    private int getDistanceReqursive(String word1, String word2, int position1, int position2,int accum) {
        if (position1 == -1 && position2 == -1) {
            return 0;
        }
        if (position1 == -1 || position2 == -1) {
            return Math.max(position1+1, position2+1);
        }
        if (accum > Math.max(word1.length(), word2.length())) {
            return Math.max(word1.length(), word2.length());
        }
        if (word1.charAt(position1) == word2.charAt(position2)) {
            return getDistanceReqursive(word1,word2,position1-1,position2-1,accum);
        }
        int result = getDistanceReqursive(word1,word2,position1,position2-1,accum+1)+1;
        result = Math.min(result, getDistanceReqursive(word1,word2,position1-1,position2,accum+1)+1);
        result = Math.min(result,getDistanceReqursive(word1,word2,position1-1,position2-1,accum+1)+1);
        if (position1 > 0 && position2 > 0 && word1.charAt(position1) == word2.charAt(position2-1) && word1.charAt(position1-1) == word2.charAt(position2)) {
            result = Math.min(result,getDistanceReqursive(word1, word2, position1 - 2, position2 - 2, accum + 1) + 1);
        }
        return result;
    }
}

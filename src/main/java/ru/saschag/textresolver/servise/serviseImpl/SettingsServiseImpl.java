package ru.saschag.textresolver.servise.serviseImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.saschag.textresolver.entity.Settings;
import ru.saschag.textresolver.repository.SettingsRepository;
import ru.saschag.textresolver.servise.SettingsServise;
import java.util.List;

/**
 * Created by galanov.ag on 30.05.2019.
 */
@Service
public class SettingsServiseImpl implements SettingsServise {
    @Autowired
    SettingsRepository repository;

    @Override
    public Settings getSettings(String name) {
        return repository.findOne(name);
    }

    @Override
    public void setSettings(Settings settings) {
        repository.save(settings);
    }

    @Override
    public List<Settings> getAll() {
        return repository.findAll();
    }
}

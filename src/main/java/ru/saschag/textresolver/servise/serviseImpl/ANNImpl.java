package ru.saschag.textresolver.servise.serviseImpl;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.learning.LearningRule;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.nnet.learning.PerceptronLearning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.saschag.textresolver.DTO.LearningResult;
import ru.saschag.textresolver.entity.Dictionary;
import ru.saschag.textresolver.entity.Settings;
import ru.saschag.textresolver.entity.Text;
import ru.saschag.textresolver.entity.TextClass;
import ru.saschag.textresolver.servise.*;
import java.io.*;
import java.util.*;

@Service
public class ANNImpl implements ANN {
    @Autowired
    private DictionaryServise dictionaryServise;

    @Autowired
    private TextClassServise classServise;

    @Autowired
    private TextServise textServise;

    @Autowired
    private SettingsServise settingsServise;

    private NeuralNetwork ann;

    public ANNImpl() {
    }

    @Override
    public void init() {
        int classCount = classServise.getClassCount();
        int wordCount = dictionaryServise.getDictionaryCount();
        Settings layersCountSettings = settingsServise.getSettings(SettingsServise.NAME_LAYERS_COUNT);
        int layersCount = 3;
        if (layersCountSettings != null) {
            layersCount = Integer.parseInt(layersCountSettings.getData());
        }
        int[] layersStructure = new int[layersCount];
        layersStructure[0] = wordCount;
        layersStructure[layersCount-1] = classCount;
        for (int i = 1; i < layersCount-1; i++) {
            layersStructure[i] = (layersStructure[i-1]+classCount)/2;
        }
        ann = new MultiLayerPerceptron(layersStructure);
        ann.randomizeWeights();
    }

    @Override
    public String train() {
        if (ann == null) {
            init();
        }
        int inputCount = ann.getInputsCount();
        int outputCount = ann.getOutputsCount();
        DataSet trainSet = new DataSet(inputCount,outputCount);
        List<Text> texts = textServise.getAll();
        texts.forEach(text -> {
            double[] input = new double[inputCount];
            double[] output = new double[outputCount];
            output[text.getTextClass().getOrder()] = 1;
            text.getWords().forEach(dictionary -> input[dictionary.getOrder()] = 1);
            trainSet.addRow(input,output);
        });
        BackPropagation learningRule = (BackPropagation) ann.getLearningRule();
        Settings maxIterationSettings = settingsServise.getSettings(SettingsServise.NAME_LEARNING_MAX_ITERATIONS);
        int maxIteration = 1000;
        if (maxIterationSettings != null) {
            maxIteration = Integer.parseInt(maxIterationSettings.getData());
        }
        learningRule.setMaxIterations(maxIteration);
        learningRule.addListener(this::eventListener);
        final String[] result = new String[1];
        learningRule.addListener(learningEvent -> {
            if (learningEvent.getEventType() == LearningEvent.Type.LEARNING_STOPPED) {
                result[0] = getIterationInfo((BackPropagation) learningEvent.getSource());
            }
        });
        ann.learn(trainSet);

        //save ann data to settings
        try {
            Settings annData = new Settings();
            annData.setData(saveAnn());
            annData.setName(SettingsServise.NAME_ANN_DATA);
            settingsServise.setSettings(annData);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result[0];
    }

    private void eventListener(LearningEvent event) {
        BackPropagation source = (BackPropagation) event.getSource();
        if (source.getCurrentIteration()%10 == 0 || event.getEventType() == LearningEvent.Type.LEARNING_STOPPED) {
            System.out.println(getIterationInfo(source));
        }

    }

    private String getIterationInfo(BackPropagation source) {
        StringBuilder result = new StringBuilder();
        result.append("{\"iteration\": ").append(source.getCurrentIteration()).append(", \"error\": ").append(source.getPreviousEpochError()).append("}");
        return result.toString();
    }

    @Override
    public LearningResult getResult(Text input) {
        if (ann == null) {
            Settings annData = settingsServise.getSettings(SettingsServise.NAME_ANN_DATA);
            if (annData == null) {
                return null;
            }
            try {
                loadAnn(annData.getData());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
        double[] inputLayer = new double[ann.getInputsCount()];
        List<Dictionary> dictionaries = dictionaryServise.getDictionatiesByWordsList(Arrays.asList(textServise.getArrayOfBigramm(input, true)));
        for (Dictionary dictionary : dictionaries) {
            inputLayer[dictionary.getOrder()] = 1;
        }
        ann.setInput(inputLayer);
        ann.calculate();

        LearningResult result = new LearningResult();
        HashMap<UUID, Double> resultMap = result.getResult();
        List<TextClass> classes = classServise.getAll();
        for (TextClass textClass : classes) {
            resultMap.put(textClass.getId(), ann.getOutput()[textClass.getOrder()]);
        }
        return result;
    }

    @Override
    public String saveAnn() throws IOException {
        if (ann == null) {
            throw new IllegalArgumentException("ann haven't been initialized");
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(ann);
        oos.close();
        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }

    @Override
    public void loadAnn(String annData) throws IOException, ClassNotFoundException {
        byte [] data = Base64.getDecoder().decode(annData);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        ann = (NeuralNetwork) ois.readObject();
        ois.close();
    }
}

package ru.saschag.textresolver.servise.serviseImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.saschag.textresolver.entity.TextClass;
import ru.saschag.textresolver.repository.TextClassRepository;
import ru.saschag.textresolver.servise.TextClassServise;
import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
@Service
public class TextClassServiseImpl implements TextClassServise {
    @Autowired
    TextClassRepository repository;

    @Override
    public TextClass save(TextClass textClass) {
        if (textClass.isEmpty()) {
            return null;
        }

        if (textClass.getOrder() == 0) {
            List<TextClass> existed = repository.getTopClass();
            if (existed.isEmpty()) {
                textClass.setOrder(0);
            } else {
                textClass.setOrder(existed.get(0).getOrder()+1);
            }
        }
        return repository.save(textClass);
    }

    @Override
    public List<TextClass> getAll() {
        return repository.findAll();
    }

    @Override
    public TextClass getById(UUID uid) {
        return repository.findOne(uid);
    }

    @Override
    public int getClassCount() {
        List<TextClass> queryResult = repository.getTopClass();
        if (queryResult.isEmpty()) {
            return 0;
        } else {
            return queryResult.get(0).getOrder()+1;
        }
    }
}

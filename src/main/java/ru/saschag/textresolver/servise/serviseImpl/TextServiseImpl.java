package ru.saschag.textresolver.servise.serviseImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.saschag.textresolver.entity.Dictionary;
import ru.saschag.textresolver.entity.Text;
import ru.saschag.textresolver.repository.TextRepository;
import ru.saschag.textresolver.servise.DictionaryServise;
import ru.saschag.textresolver.servise.LevenshteinDistance;
import ru.saschag.textresolver.servise.PottersStreamer;
import ru.saschag.textresolver.servise.TextServise;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
@Service
public class TextServiseImpl implements TextServise {
    @Autowired
    LevenshteinDistance levenshteinDistance;

    @Autowired
    PottersStreamer streamer;

    @Autowired
    private TextRepository repository;

    @Autowired
    private DictionaryServise dictionaryServise;

    @Override
    public Text save(Text text) {
        if (text.isEmpty()) {
            return null;
        }
        return repository.save(text);
    }

    @Override
    public List<Text> getAll() {
        return repository.findAll();
    }

    @Override
    public Text getById(UUID id) {
        return repository.findOne(id);
    }

    @Override
    public List<Text> getTextsWithWords() {
        return repository.getTextsWithWords();
    }

    @Override
    public void clearWords(Text text) {
        text.setWords(new ArrayList<Dictionary>());
        repository.save(text);
    }

    @Override
    public void saveList(Text[] texts) {
        for(Text text : texts) {
            repository.save(text);
        }
    }

    @Override
    public String[] getArrayOfBigramm(Text text, boolean withCorrection) {
        String[] source = createFilteredUnigrams(text, withCorrection);
        String[] result = new String[2*source.length-1];
        for (int i = 0; i < source.length-1; i++) {
            result[2*i] = source[i]+"|"+source[i+1];
            result[2*i+1] = source[i];
        }
        result[result.length-1] = source[source.length-1];
        return result;
    }

    private String[] createFilteredUnigrams(Text text, boolean withCorrection) {
        String[] result = clean(text.getText()).split("[ \n\t\r$+<>№=]");
        //streamer
        result = Arrays.stream(result).filter(word -> !word.isEmpty() && word.length() > 1).toArray(String[]::new);

        List<Dictionary> dictionary = dictionaryServise.getAllUnigramm();
        for (int i = 0; i < result.length; i++) {
            result[i] = streamer.stem(result[i]);

            //autocorrection: if less than 1 error for 4 letters - it's ok
            if (withCorrection && result[i].length() >= 4 && dictionaryServise.getByWord(result[i]) == null) {
                int bestDistance = 1000;
                String bestWord = null;
                for (Dictionary word: dictionary) {
                    if (word.getWord().length() >= result[i].length()*3/4 && word.getWord().length() <= result[i].length()*5/4) {
                        int currentDistance = levenshteinDistance.getDistance(word.getWord(), result[i]);
                        if (currentDistance < bestDistance) {
                            bestDistance = currentDistance;
                            bestWord = word.getWord();
                        }
                    }
                }
                if (bestDistance/result[0].length() <= 0.25 && bestWord != null) {
                    result[i] = bestWord;
                }
            }
        }
        return result;
    }

    private String clean(String text) {
        if (text != null) {
            return text.toLowerCase().replaceAll("[\\pP\\d]", " ");
        } else {
            return "";
        }
    }
}

package ru.saschag.textresolver.servise.serviseImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.saschag.textresolver.entity.Dictionary;
import ru.saschag.textresolver.entity.Settings;
import ru.saschag.textresolver.entity.Text;
import ru.saschag.textresolver.repository.DictionaryRepository;
import ru.saschag.textresolver.servise.DictionaryServise;
import ru.saschag.textresolver.servise.SettingsServise;
import ru.saschag.textresolver.servise.TextServise;

import java.util.*;

@Service
public class DictionaryServiseImpl implements DictionaryServise {
    @Autowired
    DictionaryRepository repository;

    @Autowired
    TextServise textServise;

    @Autowired
    SettingsServise settingsServise;

    @Transactional
    @Override
    public void refillDictionary() {
        deleteOldData();
        List<Text> texts = textServise.getAll();
        Map<String,Dictionary> mapDictionaryToWrite = new HashMap<String,Dictionary>();
        Map<String,Set<Text>> mapTextToWrite = new HashMap<String,Set<Text>>();
        texts.forEach(text -> fillByText(text, mapDictionaryToWrite, mapTextToWrite));
        writeDictionaryToBD(mapDictionaryToWrite, mapTextToWrite);
    }

    private void deleteOldData() {
        List<Text> texts = textServise.getTextsWithWords();
        texts.forEach(text -> textServise.clearWords(text));
        repository.deleteAll();
    }

    private void writeDictionaryToBD(Map<String, Dictionary> mapDictionaryToWrite, Map<String, Set<Text>> mapTextToWrite) {
        Settings dictionaryBarrierSettings = settingsServise.getSettings(SettingsServise.NAME_DICTIONARY_BARRIER);
        int dictionaryBarrier = 3;
        if (dictionaryBarrierSettings != null) {
            dictionaryBarrier = Integer.parseInt(dictionaryBarrierSettings.getData());
        }

        int orderCounter = 0;
        for (Map.Entry<String,Dictionary> pair : mapDictionaryToWrite.entrySet()) {
            String bigramm = pair.getKey();
            Dictionary dictionary = pair.getValue();
            if (dictionary.getCount() >= dictionaryBarrier) {
                dictionary.setOrder(orderCounter++);
                dictionary = repository.save(dictionary);
                Set<Text> texts = mapTextToWrite.get(bigramm);
                if (texts != null) {
                    Dictionary finalDictionary = dictionary;
                    texts.forEach(text -> {
                        text = textServise.getById(text.getId());
                        text.addDictionary(finalDictionary);
                        textServise.save(text);
                    });
                }
            }
            //TODO change loop in loop to two loop
        }
    }

    @Override
    public int getDictionaryCount() {
        List<Dictionary> queryResult = repository.getTopDictionary();
        if (queryResult.isEmpty()) {
            return 0;
        } else {
            return queryResult.get(0).getOrder() + 1;
        }
    }

    @Override
    public List<Dictionary> getDictionatiesByWordsList(List<String> words) {
        return repository.findByWordIn(words);
    }

    @Override
    public Dictionary getByWord(String word) {
        return repository.findByWord(word);
    }

    @Override
    public List<Dictionary> getAllUnigramm() {
        return repository.getAllUnigramm();
    }

    private void fillByText(Text text, Map<String,Dictionary> mapDictionaryToWrite, Map<String,Set<Text>> mapTextToWrite) {
        String[] bigramms = textServise.getArrayOfBigramm(text, false);
        for (String bigramm: bigramms) {
            Dictionary word = mapDictionaryToWrite.get(bigramm); //repository.findByWord(bigramm);
            if (word == null) {
                word = new Dictionary();
                word.setWord(bigramm);
                mapDictionaryToWrite.put(bigramm, word);
            }
            word.increaseCount();
            Set<Text> bigrammsTexts = mapTextToWrite.get(bigramm);
            if (bigrammsTexts == null) {
                bigrammsTexts = new HashSet<Text>();
                bigrammsTexts.add(text);
                mapTextToWrite.put(bigramm, bigrammsTexts);
            } else {
                bigrammsTexts.add(text);
            }
        }
    }
}
package ru.saschag.textresolver.servise;

import ru.saschag.textresolver.entity.TextClass;
import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
public interface TextClassServise {
    TextClass save(TextClass text);
    List<TextClass> getAll();
    TextClass getById(UUID uid);
    int getClassCount();
}

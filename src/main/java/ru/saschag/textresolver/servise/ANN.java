package ru.saschag.textresolver.servise;

import ru.saschag.textresolver.DTO.LearningResult;
import ru.saschag.textresolver.entity.Text;

import java.io.IOException;

public interface ANN {
    void init();
    String train();
    LearningResult getResult(Text input);
    String saveAnn() throws IOException;
    void loadAnn(String annData) throws IOException, ClassNotFoundException;
}

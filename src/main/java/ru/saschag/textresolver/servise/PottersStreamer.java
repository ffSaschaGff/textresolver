package ru.saschag.textresolver.servise;

/**
 * Created by galanov.ag on 29.05.2019.
 */
public interface PottersStreamer {
    String stem(String word);
}

package ru.saschag.textresolver.servise;

import ru.saschag.textresolver.entity.Dictionary;

import java.util.List;

public interface DictionaryServise {
    void refillDictionary();
    int getDictionaryCount();
    List<Dictionary> getDictionatiesByWordsList(List<String> words);
    Dictionary getByWord(String word);
    List<Dictionary> getAllUnigramm();
}

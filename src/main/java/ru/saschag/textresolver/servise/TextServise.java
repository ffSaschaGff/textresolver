package ru.saschag.textresolver.servise;

import ru.saschag.textresolver.entity.Text;

import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
public interface TextServise {
    Text save(Text text);
    List<Text> getAll();
    Text getById(UUID id);
    List<Text> getTextsWithWords();
    void clearWords(Text text);
    void saveList(Text[] texts);
    String[] getArrayOfBigramm(Text text, boolean withCorrection);
}

package ru.saschag.textresolver.servise;

/**
 * Created by galanov.ag on 03.06.2019.
 */
public interface LevenshteinDistance {
    int getDistance(String word1, String word2);
}

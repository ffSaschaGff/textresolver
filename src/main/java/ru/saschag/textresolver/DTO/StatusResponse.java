package ru.saschag.textresolver.DTO;

/**
 * Created by galanov.ag on 31.05.2019.
 */
public class StatusResponse {
    public static final StatusResponse OK = new StatusResponse("ok");
    public static final StatusResponse FINISHED = new StatusResponse("finished");

    private String status;

    public StatusResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

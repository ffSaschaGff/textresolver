package ru.saschag.textresolver.DTO;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by galanov.ag on 31.05.2019.
 */
public class LearningResult {

    private HashMap<UUID, Double> result = new HashMap<>();

    public HashMap<UUID, Double> getResult() {
        return result;
    }

    public void setResult(HashMap<UUID, Double> result) {
        this.result = result;
    }
}

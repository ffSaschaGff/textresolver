package ru.saschag.textresolver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.saschag.textresolver.entity.TextClass;
import ru.saschag.textresolver.servise.TextClassServise;
import java.util.List;

@RestController
public class ClassController {
    @Autowired
    TextClassServise servise;

    @RequestMapping(path = "/class/add")
    public TextClass add(@RequestBody TextClass textClass) {
        return servise.save(textClass);
    }

    @RequestMapping(path = "/class/getAll")
    public List<TextClass> getAll() {
        return servise.getAll();
    }
}

package ru.saschag.textresolver.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.saschag.textresolver.DTO.StatusResponse;

/**
 * Created by galanov.ag on 28.05.2019.
 */
@RestController
public class Main {

    @RequestMapping(path = "/test")
    public StatusResponse test() {
        return StatusResponse.OK;
    }
}

package ru.saschag.textresolver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.saschag.textresolver.DTO.LearningResult;
import ru.saschag.textresolver.DTO.StatusResponse;
import ru.saschag.textresolver.entity.Text;
import ru.saschag.textresolver.servise.ANN;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class ANNController {
    @Autowired
    private ANN ann;

    @RequestMapping(path = "/ann/init")
    public StatusResponse init() {
        ann.init();
        return StatusResponse.FINISHED;
    }

    @RequestMapping(path = "/ann/train")
    public String train() {
        return ann.train();
    }

    @RequestMapping(path = "/ann/calculate")
    public LearningResult calculate(@RequestBody Text input) {
        return ann.getResult(input);
    }

    @RequestMapping(path = "/ann/save")
    public @ResponseBody String save() throws IOException {
        return ann.saveAnn();
    }

    @RequestMapping(path = "/ann/load")
    public StatusResponse load(HttpServletResponse response,
                       HttpServletRequest request) throws IOException, ClassNotFoundException {
        ann.loadAnn(request.getReader().lines().collect(Collectors.joining()));
        return StatusResponse.FINISHED;
    }
}

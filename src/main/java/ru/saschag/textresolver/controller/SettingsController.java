package ru.saschag.textresolver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.saschag.textresolver.DTO.StatusResponse;
import ru.saschag.textresolver.entity.Settings;
import ru.saschag.textresolver.servise.SettingsServise;
import java.util.List;

/**
 * Created by galanov.ag on 30.05.2019.
 */
@RestController
public class SettingsController {
    @Autowired
    SettingsServise servise;

    @RequestMapping(path = "/settings/getAll")
    public List<Settings> getAll() {
        return servise.getAll();
    }

    @RequestMapping(path = "/settings/add")
    public StatusResponse set(@RequestBody Settings settings) {
        servise.setSettings(settings);
        return StatusResponse.FINISHED;
    }
}

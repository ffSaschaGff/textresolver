package ru.saschag.textresolver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.saschag.textresolver.DTO.StatusResponse;
import ru.saschag.textresolver.servise.DictionaryServise;

/**
 * Created by galanov.ag on 29.05.2019.
 */
@RestController
public class DictionaryController {
    @Autowired
    DictionaryServise servise;

    @RequestMapping(path = "/dictionary/refill")
    public StatusResponse refill() {
        servise.refillDictionary();
        return StatusResponse.FINISHED;
    }
}

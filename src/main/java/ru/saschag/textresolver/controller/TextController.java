package ru.saschag.textresolver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.saschag.textresolver.DTO.StatusResponse;
import ru.saschag.textresolver.entity.Text;
import ru.saschag.textresolver.servise.TextServise;
import java.io.IOException;
import java.util.List;

/**
 * Created by galanov.ag on 28.05.2019.
 */
@RestController
public class TextController {
    @Autowired
    private TextServise servise;

    @RequestMapping(path = "/text/add")
    public Text add(@RequestBody Text text) {
        return servise.save(text);
    }

    @RequestMapping(path = "/text/getAll")
    public List<Text> getAll() {
        return servise.getAll();
    }

    @RequestMapping(path = "/text/addList")
    public StatusResponse addList(@RequestBody Text[] texts) throws IOException {
        servise.saveList(texts);
        return StatusResponse.FINISHED;
    }
}

package ru.saschag.textresolver.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by galanov.ag on 30.05.2019.
 */
@Entity
public class Settings {
    @Id
    private String name;

    @Column(columnDefinition="text")
    private String data;

    public Settings() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

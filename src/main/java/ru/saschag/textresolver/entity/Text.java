package ru.saschag.textresolver.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.saschag.textresolver.deserialization.TextDeserialization;
import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
@Entity
@Table(name = "texts")
@JsonDeserialize(using = TextDeserialization.class)
public class Text {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(columnDefinition="text")
    private String text;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<Dictionary> words;

    @ManyToOne(fetch = FetchType.EAGER)
    private TextClass textClass;

    public Text() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    public void setTextClass(TextClass textClass) {
        this.textClass = textClass;
    }

    public TextClass getTextClass() {
        return textClass;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Dictionary> getWords() {
        return words;
    }

    public void addDictionary(Dictionary dictionary) {
        this.words.add(dictionary);
    }

    public void setWords(List<Dictionary> words) {
        this.words = words;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return text == null || id == null && textClass == null && words == null;
    }
}
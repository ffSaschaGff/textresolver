package ru.saschag.textresolver.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ru.saschag.textresolver.deserialization.TextClassDeserialization;
import javax.persistence.*;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
@Entity
@Table(name = "classes")
@JsonDeserialize(using = TextClassDeserialization.class)
public class TextClass {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "ann_order")
    private int order;

    @Column(name = "name")
    private String name;

    public TextClass() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return id == null && order == 0 && name == null;
    }
}

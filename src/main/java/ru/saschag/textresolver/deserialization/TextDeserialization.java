package ru.saschag.textresolver.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import ru.saschag.textresolver.entity.Text;
import ru.saschag.textresolver.servise.TextClassServise;

import java.io.IOException;
import java.util.UUID;

public class TextDeserialization extends JsonDeserializer<Text> {
    @Autowired
    private TextClassServise classServise;

    public TextDeserialization() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @Override
    public Text deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode rootNode = jsonParser.getCodec().readTree(jsonParser);
        Text text = new Text();
        if (rootNode.has("text")) {
            text.setText(rootNode.get("text").textValue());
        }
        if (rootNode.has("class")) {
            text.setTextClass(classServise.getById(UUID.fromString(rootNode.get("class").textValue())));
        }
        if (rootNode.has("id")) {
            text.setId(UUID.fromString(rootNode.get("id").textValue()));
        }
        return text;
    }
}

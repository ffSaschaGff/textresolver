package ru.saschag.textresolver.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import ru.saschag.textresolver.entity.TextClass;

import java.io.IOException;
import java.util.UUID;

public class TextClassDeserialization extends JsonDeserializer<TextClass> {

    @Override
    public TextClass deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode rootNode = jsonParser.getCodec().readTree(jsonParser);
        TextClass textClass = new TextClass();
        if (rootNode.has("name")) {
            textClass.setName(rootNode.get("name").textValue());
        }
        if (rootNode.has("order")) {
            textClass.setOrder(rootNode.get("order").intValue());
        }
        if (rootNode.has("id")) {
            textClass.setId(UUID.fromString(rootNode.get("id").textValue()));
        }
        return textClass;
    }
}

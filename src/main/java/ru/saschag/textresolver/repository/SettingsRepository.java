package ru.saschag.textresolver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.saschag.textresolver.entity.Settings;

/**
 * Created by galanov.ag on 30.05.2019.
 */
public interface SettingsRepository extends JpaRepository<Settings, String>{
}

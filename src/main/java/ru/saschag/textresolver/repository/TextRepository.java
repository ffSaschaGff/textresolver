package ru.saschag.textresolver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.saschag.textresolver.entity.Text;
import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */
public interface TextRepository extends JpaRepository<Text, UUID> {
    @Query(value = "select DISTINCT t1.* from texts as t1 inner join texts_dictionary as t2 on t1.id = t2.text_id", nativeQuery = true)
    List<Text> getTextsWithWords();
}

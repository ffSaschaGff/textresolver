package ru.saschag.textresolver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.saschag.textresolver.entity.Dictionary;
import java.util.List;
import java.util.UUID;

public interface DictionaryRepository extends JpaRepository<Dictionary, UUID> {
    @Query(value = "select * from dictionary order by dictionary.ann_order desc limit 1", nativeQuery = true)
    List<Dictionary> getTopDictionary();

    Dictionary findByWord(String word);

    List<Dictionary> findByWordIn(List<String> words);

    @Query(value = "select * from dictionary where not dictionary.word like '%|%'", nativeQuery = true)
    List<Dictionary> getAllUnigramm();
}

package ru.saschag.textresolver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.saschag.textresolver.entity.TextClass;

import java.util.List;
import java.util.UUID;

/**
 * Created by galanov.ag on 28.05.2019.
 */

public interface TextClassRepository extends JpaRepository<TextClass, UUID> {
    @Query(value = "select * from classes order by classes.ann_order desc limit 1", nativeQuery = true)
    List<TextClass> getTopClass();
}
